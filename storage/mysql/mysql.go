// Package mysql storage is designed to give lazy load singleton access to mysql connections
// it doesn't provide any cluster nor balancing support, assuming it is handled
// in lower level infra, i.e. proxy, cluster etc.
package mysql

import (
	"fmt"
	"os"
	"sync"
	"time"

	"bitbucket.org/asxce/nucleo/config"
	_ "github.com/go-sql-driver/mysql" // use by sqlx
	"github.com/jmoiron/sqlx"
	sqlmock "github.com/DATA-DOG/go-sqlmock"
)

var dbConnection *SQLConn

type SQLConn struct {
	sqlx   map[string]*sqlx.DB
	Mock   sqlmock.Sqlmock
	mux    sync.Mutex
	sqlDB  *sqlx.DB
	Multi  bool
	isMock bool
}

// DB struct
type Config struct {
	User        string
	Password    string
	Address     string
	DB          string
	LogMode     bool
	MaxOpen     int
	MaxIdle     int
	MaxLifetime int
}

func (db *SQLConn) Get(id string) *sqlx.DB {
	db.mux.Lock()
	defer db.mux.Unlock()
	if conn, ok := db.sqlx[id]; ok {
		return conn
	}
	return nil
}

func (db *SQLConn) Set(id string, sqlx *sqlx.DB) {
	db.mux.Lock()
	db.sqlx[id] = sqlx
	db.mux.Unlock()
}

// New : initialize new DB connections, if dbConnection is already exists, returns that instead
func New(isMock bool) *SQLConn {
	if dbConnection == nil {
		dbConnection = &SQLConn{sqlx: make(map[string]*sqlx.DB)}
	}

	if isMock {
		d, mock, err := sqlmock.New()
		if err != nil {
			return nil
		}

		db := sqlx.NewDb(d, "mysql")
		return &SQLConn{Mock: mock, sqlDB: db, isMock: true}
	}

	return dbConnection
}

// MustConnect retrieve MySQL established connection client (sqlx) and panic if error
func (db *SQLConn) MustConnect(id string) *sqlx.DB {
	d, err := db.Connect(id)
	if err != nil {
		panic(err)
	}
	return d
}

// Connect retrieve MySQL established connection client (sqlx)
func (db *SQLConn) Connect(id string) (*sqlx.DB, error) {
	mysqlConfig := config.GetStringMap("mysql")
	if _, ok := mysqlConfig[id]; !ok {
		return nil, fmt.Errorf("mysql configuration for [%s] does not exists", id)
	}

	if db.Multi {
		return db.openConnection(id)
	}

	if db.isMock {
		return db.sqlDB, nil
	}

	// if previously established, reuse and ping
	if con := db.Get(id); con != nil {
		return con, nil
	}

	con, err := db.openConnection(id)
	if err != nil {
		return nil, err
	}

	// otherwise establish new connection through centralized component config
	db.Set(id, con)
	return db.Get(id), nil
}

// Shutdown disconnecting all established mysql client connection
func Shutdown() (err error) {
	if dbConnection == nil {
		return nil
	}
	for _, c := range dbConnection.sqlx {
		err = c.Close()
	}
	return err
}

func (db *SQLConn) openConnection(id string) (*sqlx.DB, error) {
	opt := setupConfig(id)
	dsn := fmt.Sprintf(
		"%v:%v@tcp(%v)/%v?charset=utf8&parseTime=True&loc=Local",
		opt.User,
		opt.Password,
		opt.Address,
		opt.DB,
	)

	// override configuration with environment variable
	host := os.Getenv("MYSQL_CONFIG")
	if host != "" {
		dsn = host
	}

	if db.Multi {
		dsn += "&multiStatements=true"
	}

	con, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		fallback := config.GetString(getKey(id, "fallback_to"))
		if fallback == id { // prevent endless loop
			return db.Get(id), err
		}
		return db.Connect(fallback)
	}

	con.SetConnMaxLifetime(time.Duration(opt.MaxLifetime))
	con.SetMaxOpenConns(opt.MaxOpen)
	con.SetMaxIdleConns(opt.MaxIdle)

	return con, nil
}

func setupConfig(id string) *Config {
	option := &Config{
		User:        config.GetString(getKey(id, "user")),
		Password:    config.GetString(getKey(id, "password")),
		Address:     config.GetString(getKey(id, "address")),
		DB:          config.GetString(getKey(id, "db")),
		MaxLifetime: config.GetInt(getKey(id, "max_lifetime")),
		MaxOpen:     config.GetInt(getKey(id, "max_open")),
		MaxIdle:     config.GetInt(getKey(id, "max_idle")),
		LogMode:     config.GetBool(getKey(id, "log")),
	}

	if option.MaxLifetime == 0 {
		option.MaxLifetime = 30
	}

	if option.MaxOpen == 0 {
		option.MaxOpen = 30
	}
	return option
}

func getKey(id, types string) string {
	return fmt.Sprintf("mysql.%s.%s", id, types)
}
