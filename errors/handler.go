package errors

import (
	"database/sql"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/labstack/echo/v4"
)

// ErrorHandler override echo.HTTPErrorHandler
func ErrorHandler(err error, c echo.Context) {
	var e = &Errors{c}

	switch err.(type) {
	case *APIError:
		c.JSON(err.(*APIError).StatusCode(), err)
	case validation.Errors:
		c.JSON(http.StatusBadRequest, e.InvalidData(err.(validation.Errors)))
	case *echo.HTTPError:
		errEcho := err.(*echo.HTTPError)

		if errEcho.Message == sql.ErrNoRows {
			c.JSON(http.StatusNotFound, e.NotFound("the requested resource"))
			return
		}
		switch errEcho.Code {
		case http.StatusUnauthorized:
			c.JSON(http.StatusUnauthorized, e.Unauthorized(err.Error()))
		case http.StatusNotFound, http.StatusMethodNotAllowed:
			c.JSON(http.StatusNotFound, e.NotFound("the requested resource"))
		default:
			c.JSON(errEcho.Code, NewAPIError(c, errEcho.Code, http.StatusText(errEcho.Code), Params{"error": errEcho.Message}))
		}
	default:
		c.JSON(http.StatusInternalServerError, e.InternalServerError(err))
	}
}
