package errors

import (
	"net/http"
	"sort"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/labstack/echo/v4"
)

// validationError initialization
type validationError struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

// Errors initialization
type Errors struct {
	ctx echo.Context
}

// InternalServerError creates a new API error representing an internal server error (HTTP 500)
func (e *Errors) InternalServerError(err error) *APIError {
	code := http.StatusInternalServerError
	apiError := NewAPIError(e.ctx, code, http.StatusText(code), Params{"error": err})
	return apiError
}

// NotFound creates a new API error representing a resource-not-found error (HTTP 404)
func (e *Errors) NotFound(resource string) *APIError {
	code := http.StatusNotFound
	apiError := NewAPIError(e.ctx, code, http.StatusText(code), Params{"resource": resource})
	return apiError
}

// Unauthorized creates a new API error representing an authentication failure (HTTP 401)
func (e *Errors) Unauthorized(err string) *APIError {
	code := http.StatusUnauthorized
	apiError := NewAPIError(e.ctx, code, http.StatusText(code), Params{"error": err})
	return apiError
}

// InvalidData converts a data validation error into an API error (HTTP 400)
func (e *Errors) InvalidData(errs validation.Errors) *APIError {
	result := make([]validationError, 0)
	fields := make([]string, 0)
	for field := range errs {
		fields = append(fields, field)
	}
	sort.Strings(fields)
	for _, field := range fields {
		err := errs[field]
		result = append(result, validationError{
			Field: field,
			Error: err.Error(),
		})
	}

	err := NewAPIError(e.ctx, http.StatusBadRequest, "Invalid Data", nil)
	err.Details = result

	return err
}
