# Nucleo Package
Nucleo is a standard toolkit to building API services in GO, with intention to solve common problem in distributed system.

# Go Version 1.12.4
Management Project and Dependency using Go Modules

# Components
- Viper, config management.
- Echo v4, web framework.
- SQLX, an sql library.